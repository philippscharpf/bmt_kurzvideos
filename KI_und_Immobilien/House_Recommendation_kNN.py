######################
# BMT KI/ML Kurzvideos
# Philipp Scharpf
#################

# Folge: KI und Immobilien
# Berechnung von Haus-Empfehlungen
# mit Machine Learning (k Nearest Neighbors Algorithmus)

# Datensatz: Datei "houses.csv" = "train.csv" von
# https://www.kaggle.com/c/house-prices-advanced-regression-techniques

import pandas as pd

# read in csv file as pandas data frame
data_frame = pd.read_csv("houses.csv")

# select features
selection = data_frame[["Id","Neighborhood","SalePrice"]]
selection = selection[:25][["Neighborhood","SalePrice"]]

# plot data
import matplotlib.pyplot as plt
selection.plot(kind='scatter',x='Neighborhood',y='SalePrice')
for idx in selection.index:
    x = selection["Neighborhood"][idx]
    y = selection["SalePrice"][idx]
    text = str(idx)
    plt.text(x,y,text)
plt.title("House recommendations")
plt.show()

# labels to numbers
from sklearn.preprocessing import LabelEncoder
selection["Neighborhood"] = LabelEncoder().fit_transform(selection["Neighborhood"])

# compute k nearest neighbors
from sklearn.neighbors import NearestNeighbors
nr_recommendations = 3
nbrs = NearestNeighbors(n_neighbors=nr_recommendations+1).fit(selection)
distances, indices = nbrs.kneighbors(selection)
recommendations = indices

# print array with header [house, rec 1, rec 2, rec 3]
print("house-recommendations = \n" + str(recommendations))