######################
# BMT KI/ML Kurzvideos
# Philipp Scharpf
#################

# Folge: KI und Risiko
# Berechnung von maximalen Verlusten (Drawdowns)
# f�r zwei ausgew�hlte Fonds (S&P 500 und MSCI China ETF)

import pandas as pd
import yfinance as yf
import matplotlib.pyplot as plt

etfs = [("iShares S&P 500 ETF","EX20.VI"),("iShares MSCI China ETF","MCHI")]

# define drawdown function
def drawdown(return_series: pd.Series):
    """Takes a time series of asset returns.
       returns a DataFrame with columns for
       the wealth index,
       the previous peaks, and
       the percentage drawdown
       (c) Vijay Vaidyanathan 2019, EDHEC Risk Kit Code
    """
    wealth_index = 1000*(1+return_series).cumprod()
    previous_peaks = wealth_index.cummax()
    drawdowns = (wealth_index - previous_peaks)/previous_peaks
    return pd.DataFrame({"Wealth": wealth_index,
                         "Previous Peak": previous_peaks,
                         "Drawdown": drawdowns})

# assess etf risk
for etf in etfs:
    # get name and index
    etf_name = etf[0]
    etf_index = etf[1]
    # get ticker
    ticker = yf.Ticker(etf_index)
    etf_values = ticker.history(period="max")["Close"]
    # plot ticker
    etf_values.plot()
    # get return and drawdown
    return_series = etf_values.pct_change()
    max_drawdown = max(abs(drawdown(return_series)["Drawdown"].dropna()))
    print("Max drawdown(" + etf_name + "): " + str(max_drawdown))

# show plot
plt.title("ETF risk comparison")
plt.legend([etf[0] for etf in etfs])
plt.show()