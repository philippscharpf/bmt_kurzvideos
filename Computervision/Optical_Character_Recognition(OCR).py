######################
# BMT KI/ML Kurzvideos
# Philipp Scharpf
#################

# Folge: Computervision
# Optical Character Recognition (OCR) auf MNIST-Datensatz
# Editierte Version von
# https://towardsdatascience.com/image-classification-in-10-minutes-with-mnist-dataset-54c35b77a38d

# Lade train/test-Split des MNIST-Datensatz
import tensorflow as tf
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
# Reshape Bildvektoren für Keras
input_shape = (28, 28, 1)
x_train = x_train.reshape(x_train.shape[0], input_shape[0], input_shape[1], input_shape[2])
x_test = x_test.reshape(x_test.shape[0], input_shape[0], input_shape[1], input_shape[2])
# Normalisiere RGB-Codes mit Division durch Maximalwert
value_type, max_RGB = 'float32', 255
x_train = x_train.astype(value_type) / max_RGB
x_test = x_test.astype(value_type) / max_RGB

# Erstelle Sequential Model mit verschiedenen Layers
from keras.models import Sequential
from keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPooling2D
model = Sequential()
model.add(Conv2D(28, kernel_size=(3,3), input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(128, activation=tf.nn.relu))
model.add(Dropout(0.2))
model.add(Dense(10,activation=tf.nn.softmax))

# Kompiliere und fitte Modell
model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
model.fit(x=x_train,y=y_train, epochs=10)
# Evaluiere Modell auf Testset
model.evaluate(x_test, y_test)

# Teste zufälliges Zahlenbild
import numpy as np
import matplotlib.pyplot as plt
image_index = np.random.randint(y_test.size)
plt.imshow(x_test[image_index].reshape(28, 28),cmap='Greys')
plt.show()
pred = model.predict(x_test[image_index].reshape(1, 28, 28, 1))
print("Testset Image No. " + str(image_index) + ": " + str(pred.argmax()))